package Ex1;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ReadFile extends Thread{

    private String file;
    public ReadFile(String file){
        this.file = file;
    }

    @Override
    public void run() {
        openAndReadFile();
    }

    private void openAndReadFile() {
        int countLines = 0;
        String line;
        File fRead = new File(file);
        try {
            Scanner scFRead = new Scanner(fRead);
            while (scFRead.hasNextLine()) {
                line = scFRead.nextLine();
                countLines++;
            }
            scFRead.close();
            System.out.println(countLines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}